/*
 * TEST FOR ALL EX
 */

#include <unistd.h>
#include "ex00/ft_putstr.c"
#include "ex01/ft_putnbr.c"
#include <stdio.h>
#include <limits.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		main(void)
{
	char str[] = "Hello";

	// Exercise 00 : ft_putstr
	puts("Should be Hello:");
	ft_putstr(str);
	puts("\n");

	// Exercise 01: ft_putnbr
	puts("Should be 0, 7, 17, -17, -7777777:");
	ft_putnbr(0);
	ft_putnbr(7);
	ft_putnbr(17);
	ft_putnbr(-17);
	ft_putnbr(-7777777);
	ft_putnbr(-2147483647);
	
	// -2147483648
	int max = INT_MAX;
	int min = INT_MIN;
	ft_putnbr(max);
	ft_putnbr(min);

	return (0);
}
